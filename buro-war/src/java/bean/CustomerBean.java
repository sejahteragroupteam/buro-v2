/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Customer;
import entity.Customer;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityTransaction;
import org.primefaces.event.RowEditEvent;
import session.CustomerEJB;
import session.CustomerEJBRemote;

/**
 *
 * @author Rangga
 */
@Named(value = "customerBean")
@SessionScoped
public class CustomerBean implements Serializable {

    @EJB
    private CustomerEJBRemote custEJB;
    private Customer cust;
    private List<Customer> custList = new ArrayList<>();

    String alamat;
    String email;
    String kota;
    String namaBelakang;
    String namaDepan;
    String noTlp;
    String username;
    String password;

    public CustomerBean() {
    }

    public List<Customer> getCustList() {
        custList = custEJB.findCustomers();
        return custList;
    }

    public CustomerEJBRemote getCustEJB() {
        return custEJB;
    }

    public void setCustEJB(CustomerEJBRemote custEJB) {
        this.custEJB = custEJB;
    }

    public Customer getCust() {
        return cust;
    }

    public void setCust(Customer cust) {
        this.cust = cust;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getNamaBelakang() {
        return namaBelakang;
    }

    public void setNamaBelakang(String namaBelakang) {
        this.namaBelakang = namaBelakang;
    }

    public String getNamaDepan() {
        return namaDepan;
    }

    public void setNamaDepan(String namaDepan) {
        this.namaDepan = namaDepan;
    }

    public String getNoTlp() {
        return noTlp;
    }

    public void setNoTlp(String noTlp) {
        this.noTlp = noTlp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String addCustomer() {
        custList = custEJB.findCustomers();
        for (Customer cxx : custList) {
            if (String.valueOf(username).equalsIgnoreCase(cxx.getUsername())) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Username Sudah Dipakai!", "");
                FacesContext.getCurrentInstance().addMessage(null, message);
            } else {
                FacesMessage message = new FacesMessage("Registrasi berhasil Silahkan login ulang!");
                Customer c = new Customer();
                c.setNamaDepan(namaDepan);
                c.setNamaBelakang(namaBelakang);
                c.setAlamat(alamat);
                c.setEmail(email);
                c.setKota(kota);
                c.setNoTelp(noTlp);
                c.setPassword(password);
                c.setUsername(username);
                custEJB.createCustomer(c);
                FacesContext.getCurrentInstance().addMessage(null, message);
                reset();
                return "index.xhtml";
            }
        }
        return null;
    }

//    public void deleteCustomer(Customer customer) {
//        FacesMessage msg = new FacesMessage("Customer berhasil dihapus");
//        Customer c = custEJB.findCustomerById(customer.getIdCustomer());
//        custEJB.deleteCustomer(c);
//        FacesContext.getCurrentInstance().addMessage(null, msg);
//
//    }
    public void reset() {
        setNamaDepan("");
        setNamaBelakang("");
        setAlamat("");
        setEmail("");
        setKota("");
        setNoTlp("");
        setPassword("");
        setUsername("");
    }
}
