/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Customer;
import entity.DetailOrder;
import entity.Kategori;
import entity.Orderan;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import session.DetailOrderEJBRemote;
import session.OrderEJBRemote;
import sun.security.util.PendingException;

/**
 *
 * @author abi_v
 */
@Named(value = "beanOrderan")
@SessionScoped
public class OrderanBean implements Serializable {

    @EJB
    OrderEJBRemote orderEJB;
    @EJB
    DetailOrderEJBRemote detailOrderEJB;
    List<Orderan> orderList = new ArrayList<>();
    Orderan order;

    Integer id_order;
    Double total;
    Date tglOrder;
    Customer customer;

    List<DetailOrder> detailOrderList = new ArrayList<>();

    /**
     * Creates a new instance of beanOrderan
     */
    public OrderanBean() {

    }

    public List<Orderan> getOrderList() {
        orderList = orderEJB.findOrder();
        return orderList;
    }

    public OrderEJBRemote getOrderEJB() {
        return orderEJB;
    }

    public void setOrderEJB(OrderEJBRemote orderEJB) {
        this.orderEJB = orderEJB;
    }

    public List<DetailOrder> getDetailOrderList() {
        detailOrderList = detailOrderEJB.getListDo();
        return detailOrderList;
    }

    public void setDetailOrderList(List<DetailOrder> detailOrderList) {
        this.detailOrderList = detailOrderList;
    }

    public DetailOrderEJBRemote getDetailOrderEJB() {
        return detailOrderEJB;
    }

    public void setDetailOrderEJB(DetailOrderEJBRemote detailOrderEJB) {
        this.detailOrderEJB = detailOrderEJB;
    }

    public Orderan getOrder() {
        return order;
    }

    public void setOrder(Orderan order) {
        this.order = order;
    }

    public Integer getId_order() {
        return id_order;
    }

    public void setId_order(Integer id_order) {
        this.id_order = id_order;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getTglOrder() {
        return tglOrder;
    }

    public void setTglOrder(Date tglOrder) {
        this.tglOrder = tglOrder;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}
