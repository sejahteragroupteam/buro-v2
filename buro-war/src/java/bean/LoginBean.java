/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Customer;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import session.CustomerEJBRemote;

/**
 *
 * @author Rangga
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    String username;
    String password;

    @EJB
    CustomerEJBRemote customerEJB;

    Customer c;
    List<Customer> custList = new ArrayList<>();

    String userLog;
    String passLog;

    int id;
    String namaDepan = "";
    String alamat;
    String email;
    String kota;
    String namaBelakang;
    String noTlp;
    String userCust;
    String passCust;

    boolean loginC = false;

    public LoginBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserCust() {
        return userCust;
    }

    public void setUserCust(String userCust) {
        this.userCust = userCust;
    }

    public String getPassCust() {
        return passCust;
    }

    public void setPassCust(String passCust) {
        this.passCust = passCust;
    }

    public Customer getC() {
        return c;
    }

    public void setC(Customer c) {
        this.c = c;
    }

    public List<Customer> getCustList() {
        custList = customerEJB.findCustomers();
        return custList;
    }

    public void setListCust(List<Customer> custList) {
        this.custList = custList;
    }

    public CustomerEJBRemote getCustomerEJB() {
        return customerEJB;
    }

    public void setCustomerEJB(CustomerEJBRemote customerEJB) {
        this.customerEJB = customerEJB;
    }

    public boolean isLoginC() {
        return loginC;
    }

    public void setLoginC(boolean loginC) {
        this.loginC = loginC;
    }

    public String getUserLog() {
        return userLog;
    }

    public void setUserLog(String userLog) {
        this.userLog = userLog;
    }

    public String getPassLog() {
        return passLog;
    }

    public void setPassLog(String passLog) {
        this.passLog = passLog;
    }

    public String getNamaDepan() {
        return namaDepan;
    }

    public void setNamaDepan(String namaDepan) {
        this.namaDepan = namaDepan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getNamaBelakang() {
        return namaBelakang;
    }

    public void setNamaBelakang(String namaBelakang) {
        this.namaBelakang = namaBelakang;
    }

    public String getNoTlp() {
        return noTlp;
    }

    public void setNoTlp(String noTlp) {
        this.noTlp = noTlp;
    }

    public String logins() {
        boolean login = false;
        if (username.equalsIgnoreCase("admin") && password.equals("admin")) {
            login = true;
        } else {
            login = false;
            FacesMessage mess = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username / Password Salah", "");
            FacesContext.getCurrentInstance().addMessage(null, mess);
        }
        if (login) {
            return "admin/menuAwal.xhtml?faces-redirect=true";
        }

        return null;
    }

    public String cekLogin() {
        if (loginC == false) {
            return "CustLogin.xhtml";
        } else {
            return "CustEdit.xhtml";
        }
    }

    public String cekCart() {
        if (loginC == false) {
            return "CustLogin.xhtml";
        } else {
            return "CustCart.xhtml";
        }
    }

    public String loginCust() {
        custList = customerEJB.findCustomers();
        for (Customer cxx : custList) {
            if (String.valueOf(userLog).equalsIgnoreCase(cxx.getUsername()) && String.valueOf(passLog).equals(cxx.getPassword())) {
                loginC = true;
                Customer c = new Customer();
                c = customerEJB.findCustomerById(cxx.getIdCustomer());
                id = cxx.getIdCustomer();
                setUserLog("");
                setPassLog("");
                setNamaDepan(cxx.getNamaDepan());
                setNamaBelakang(cxx.getNamaBelakang());
                setAlamat(cxx.getAlamat());
                setEmail(cxx.getEmail());
                setKota(cxx.getKota());
                setNoTlp(cxx.getNoTelp());
                setPassCust(cxx.getPassword());
                return "index.xhtml";
            } else {
                loginC = false;
            }
        };

        if (userLog.equalsIgnoreCase("")) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Username harus diisi!", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else if (passLog.equalsIgnoreCase("")) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Password harus diisi!", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Username/Password Salah!", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    }

    public void updateCustomer() {
        FacesMessage msg = new FacesMessage("Profile berhasil diedit");
//        Customer c = new Customer();
        c = customerEJB.findCustomerById(id);

        c.setAlamat(alamat);
        c.setEmail(email);
        c.setKota(kota);
        c.setNoTelp(noTlp);
        c.setPassword(passCust);

        customerEJB.updateCustomer(c);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String logout() {
        Customer c = new Customer();
        setUserLog("");
        setPassLog("");
        setNamaDepan("");
        setNamaBelakang("");
        setAlamat("");
        setEmail("");
        setKota("");
        setNoTlp("");
        setPassCust("");
        id = -1;
        loginC = false;

        return "index.xhtml";
    }
}
