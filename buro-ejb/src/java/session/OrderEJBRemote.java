/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Orderan;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author abi_v
 */
@Remote
public interface OrderEJBRemote {

    public List<Orderan> findOrder();

    public Orderan findOrderById(Integer id);

    public Orderan createOrder(Orderan order);

    public void deleteOrder(Orderan order);

    public Orderan updateOrder(Orderan order);
    
}
