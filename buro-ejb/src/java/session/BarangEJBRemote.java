/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Barang;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author abi_v
 */
@Remote
public interface BarangEJBRemote {

    public List<Barang> findBarangs();

    public Barang findBarangById(Integer id);

    public Barang createBarang(Barang barang);

    public void deleteBarang(Barang barang);

    public Barang updateBarang(Barang barang);
    
}
