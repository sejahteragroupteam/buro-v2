/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Kategori;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author abi_v
 */
@Stateless
public class KategoriEJB implements KategoriEJBRemote {

    @PersistenceContext(unitName = "buro-ejbPU")
    private EntityManager em;
    

    @Override
    public List<Kategori> findKategoris(){
        Query query = em.createNamedQuery("findAllKategori");
        return query.getResultList();
    }

    @Override
    public Kategori findKategoriById(Integer id){
        return em.find(Kategori.class, id);
    }
    

    @Override
    public Kategori createKategori(Kategori kategori){
        em.persist(kategori);
        return kategori;
    }
    

    @Override
    public void deleteKategori(Kategori kategori){
        em.remove(em.merge(kategori));
    }
    

    @Override
    public Kategori updateKategori(Kategori kategori){
        return em.merge(kategori);
    }
}
