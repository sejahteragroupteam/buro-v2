/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Customer;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author abi_v
 */
@Remote
public interface CustomerEJBRemote {

    public List<Customer> findCustomers();

    public Customer findCustomerById(Integer id);

    public Customer createCustomer(Customer customer);

    public void deleteCustomer(Customer customer);

    public Customer updateCustomer(Customer customer);
    
}
