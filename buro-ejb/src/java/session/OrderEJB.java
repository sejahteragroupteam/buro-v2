/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Orderan;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author abi_v
 */
@Stateless
public class OrderEJB implements OrderEJBRemote {

    @PersistenceContext(unitName = "buro-ejbPU")
    private EntityManager em;

    @Override
    public List<Orderan> findOrder() {
        Query query = em.createNamedQuery("findAllOrder");
        return query.getResultList();
    }

    @Override
    public Orderan findOrderById(Integer id) {
        return em.find(Orderan.class, id);
    }

    @Override
    public Orderan createOrder(Orderan order) {
        em.persist(order);
        return order;
    }

    @Override
    public void deleteOrder(Orderan order) {
        em.remove(em.merge(order));
    }

    @Override
    public Orderan updateOrder(Orderan order) {
        return em.merge(order);
    }
}
