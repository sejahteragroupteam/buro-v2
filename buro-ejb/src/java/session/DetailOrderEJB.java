/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Barang;
import entity.Customer;
import entity.DetailOrder;
import entity.Orderan;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author abi_v
 */
@Stateful
public class DetailOrderEJB implements DetailOrderEJBRemote {

@PersistenceContext(unitName = "buro-ejbPU")
    private EntityManager em;
    private List<Barang> cartItems = new ArrayList<>();

    private List<DetailOrder> listDo = new ArrayList<>();

@Override
    public void addItem(Barang item) {
        Boolean isThere = true;
        if (listDo.isEmpty()) {
            addDetailOrder(item);
        } else {
            for (DetailOrder dom : listDo) {
                if (dom.getIdBarang().getIdBarang().equals(item.getIdBarang())) {
                    dom.setJumlah(dom.getJumlah()+ 1);
                    isThere = true;
                    break;
                } else {
                    isThere = false;
                }
            }
            if (isThere) {

            } else {
                addDetailOrder(item);
            }
        }
    }

    private void addDetailOrder(Barang item) {
        DetailOrder dod = new DetailOrder();
        dod.setIdBarang(item);
        dod.setHarga(item.getHarga());
        dod.setJumlah(1);
        listDo.add(dod);
    }

@Override
    public void removeItem(DetailOrder item) {
        DetailOrder dod = new DetailOrder();
        for (DetailOrder dom : listDo) {
            if (dom.getIdBarang().getIdBarang().equals(item.getIdBarang())) {
                dod = dom;
            }
        }
        listDo.remove(dod);
    }

@Override
    public Double getTotal() {
        if (listDo == null || listDo.isEmpty()) {
            return 0d;
        }
        Double total = 0d;
        for (DetailOrder cartItem : listDo) {
            total += (cartItem.getIdBarang().getHarga() * cartItem.getJumlah());
        }
        return total;
    }

@Override
    public void checkout(Customer c) {
        Orderan od = new Orderan();
        DetailOrder dod = new DetailOrder();
        
        List<DetailOrder> listD = new ArrayList<>();
        Double total = 0d;
        for (DetailOrder b : listDo) {
            dod.setIdBarang(b.getIdBarang());
            dod.setIdOrder(od);
            dod.setHarga(b.getHarga());
            total += b.getIdBarang().getHarga() * b.getJumlah();
            listD.add(dod);
        }

        od.setDetailOrderList(listD);
        od.setIdCustomer(c);
        od.setTotal(total);
        em.persist(od);

        listDo.removeAll(listDo);
    }

@Override
    public void empty() {
        cartItems.clear();
    }

@Override
    public List<Barang> getCartItems() {
        return cartItems;
    }


@Override
    public void setCartItems(List<Barang> cartItems) {
        this.cartItems = cartItems;
    }


@Override
    public List<DetailOrder> getListDo() {
        return listDo;
    }


@Override
    public void setListDo(List<DetailOrder> listDo) {
        this.listDo = listDo;
    }
}
