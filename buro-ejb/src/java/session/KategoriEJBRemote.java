/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Kategori;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author abi_v
 */
@Remote
public interface KategoriEJBRemote {

    public Kategori findKategoriById(Integer id);

    public List<Kategori> findKategoris();

    public Kategori createKategori(Kategori kategori);

    public void deleteKategori(Kategori kategori);

    public Kategori updateKategori(Kategori kategori);
    
}
