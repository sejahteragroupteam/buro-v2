/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author abi_v
 */
@Entity
@Table(name = "detail_order")
@NamedQuery(name = ".findAllDetailOrder", query = "SELECT d FROM DetailOrder d")
public class DetailOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_detail")
    private Integer idDetail;
    @Column(name = "harga")
    private Double harga;
    @Column(name = "jumlah")
    private Integer jumlah;
    @JoinColumn(name = "id_order", referencedColumnName = "id_order")
    @ManyToOne
    private Orderan idOrder;
    @JoinColumn(name = "id_barang", referencedColumnName = "id_barang")
    @ManyToOne
    private Barang idBarang;

    public DetailOrder() {
    }

    public DetailOrder(Integer idDetail) {
        this.idDetail = idDetail;
    }

    public Integer getIdDetail() {
        return idDetail;
    }

    public void setIdDetail(Integer idDetail) {
        this.idDetail = idDetail;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Orderan getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Orderan idOrder) {
        this.idOrder = idOrder;
    }

    public Barang getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Barang idBarang) {
        this.idBarang = idBarang;
    }

}
